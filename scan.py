import sqlite3
from datetime import datetime

def insertUser(idcert, subject, inn, uc, exp, container, keyid):
    conn = None
    try:
        conn = sqlite3.connect(r'/root/cryptobro_linux/DB/certs.db')
        cur = conn.cursor()
        conn.text_factory = str
        cur.execute(
            "Insert INTO article (idcert, subject, inn, uc, exp, container, keyid) VALUES ('{0}','{1}','{2}','{3}','{4}','{5}','{6}')".format(
                idcert, subject, inn, uc, exp, container, keyid))
        conn.commit()
        conn.close()
    except Exception as inst:
        pass
    finally:
        if conn:
            conn.close()


import re

arr = []
cnt = -1

idcert = ""
subject = ""
inn = ""
uc = ""
exp = ""
container = ""
keyid = ""
ucline = False


def return_first_match(pattern, text):
    result = re.findall(pattern, text)
    result = result[0] if result else ""
    return result


import subprocess

proc = subprocess.Popen(r'/opt/cprocsp/bin/amd64/certmgr -list', shell=True, stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE)

result = []
for line in proc.stdout:
    result.append(line.decode("utf-8"))

for line in reversed(result):
    cnt += 1

    if cnt > 3:
        if "-------" in line and not "-------," in line:
            idcert = return_first_match(r'\d+', line)
            insertUser(idcert, subject, inn, uc, exp, container, keyid)
            idcert = ""
            subject = ""
            inn = ""
            uc = ""
            exp = ""
            container = ""
            keyid = ""

        if "Container" in line:
            container = line.split('Container           :')[1].strip()

        if "Not valid after" in line:
            exp = line.split('Not valid after     :')[1].strip()
            exp = exp.replace(" UTC","")
            dateFormatter = "%d/%m/%Y %H:%M:%S"
            exp = datetime.strptime(exp, dateFormatter)



        if "SubjKeyID" in line:
            keyid = line.split(':')[1].strip()

        if ucline == True:
            uc = return_first_match(r'(?<=O=).+', line)

            if ',' in uc:
                uc = uc.split(',')[0]
            uc = uc.replace("ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ", "ООО")
            uc = uc.replace("УДОСТОВЕРЯЮЩИЙ ЦЕНТР", "УЦ")
            uc = uc.replace("Общество с ограниченной ответственностью", "ООО")
            uc = uc.replace("Открытое Акционерное Общество", "ОАО")
            uc = uc.replace("Акционерное общество", "АО")
            uc = uc.replace("Акционерное Общество", "АО")
            uc = uc.replace("АКЦИОНЕРНОЕ ОБЩЕСТВО", "АО")
            ucline = False

        if "Subject" in line:
            subject = return_first_match(r'(?<=CN=).+', line)
            if ',' or '/' in subject:
                subject = subject.split(',')[0]
                subject = subject.split('/')[0]
            subject = subject.replace("ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ", "ООО")
            subject = subject.replace("Общество с ограниченной ответственностью", "ООО")
            subject = subject.replace("Открытое Акционерное Общество", "ОАО")
            subject = subject.replace("Акционерное общество", "АО")
            subject = subject.replace("Акционерное Общество", "АО")
            subject = subject.replace("АКЦИОНЕРНОЕ ОБЩЕСТВО", "АО")
            inn = return_first_match(r'(?<=INN=).+?(?=,)', line)
            if "/KPP=" in inn:  # функция исправления ИНН
                inn = inn[:-34]



            ucline = True
        print(line.rstrip())