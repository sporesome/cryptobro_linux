import sqlite3
from flask_sqlalchemy import SQLAlchemy
from dateutil.relativedelta import relativedelta
import subprocess
import os
import zipfile
from datetime import datetime
import functools
import os.path
from werkzeug.utils import secure_filename
from flask import (Flask, flash, redirect, render_template, request, session, url_for, send_from_directory)


ADMIN_PASSWORD = 'Rocket'
SECRET_KEY = 'PdYX0iHk11NZ5nDZ8O4brIt89'
UPLOAD_FOLDER = '/var/opt/cprocsp/keys/root/'
ALLOWED_EXTENSIONS = set(['zip'])
app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///DB/certs.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['ADMIN_PASSWORD'] = ADMIN_PASSWORD
db = SQLAlchemy(app)

class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    idcert = db.Column(db.Text, nullable=True)
    subject = db.Column(db.Text, nullable=True)
    inn = db.Column(db.Text, nullable=True)
    uc = db.Column(db.Text, nullable=True)
    exp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    container = db.Column(db.Text, nullable=True)
    keyid = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return '<Article %r>' % self.id

#-----------------------------------------------------------------------------------------------------------------

def login_required(fn):
    @functools.wraps(fn)
    def inner(*args, **kwargs):
        if session.get('logged_in'):
            return fn(*args, **kwargs)
        return redirect(url_for('login', next=request.path))
    return inner

# Авторизация
@app.route('/login/', methods=['GET', 'POST'])
def login():
    next_url = request.args.get('next') or request.form.get('next')
    if request.method == 'POST' and request.form.get('password'):
        password = request.form.get('password')
        if password == app.config['ADMIN_PASSWORD']:
            session['logged_in'] = True
            session.permanent = True  # Use cookie to store session.
            flash('You are now logged in.', 'success')
            return redirect(next_url or url_for('index'))
        else:
            flash('Incorrect password.', 'danger')
    return render_template('page-login.html', next_url=next_url)

@app.route('/logout/', methods=['GET', 'POST'])
def logout():
    if request.method == 'POST':
        session.clear()
        return redirect(url_for('login'))
    return render_template('logout.html')




@app.route('/')
@app.route('/home')
@login_required
def index():
    articles = Article.query.all()
    rows = Article.query.count()
    now = datetime.utcnow()
    actualrows = rows

    for el in articles:
        if el.exp < now:
            actualrows = actualrows - 1





    return render_template('index.html', actualrows=actualrows, rows=rows, articles=articles, now=now )

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
def redirect_url(default='index'):
    return request.args.get('next') or \
           request.referrer or \
           url_for(default)

# Функция импорта кэп
@app.route('/upload', methods=['GET', 'POST'])
@login_required
def upload_file():

 if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']

        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            zip_ref = zipfile.ZipFile(os.path.join(UPLOAD_FOLDER, filename), 'r')
            zip_ref.extractall(UPLOAD_FOLDER)
            zip_ref.close()
 subprocess.Popen(r'python /root/cryptobro_linux/absorb.py', shell=True)

 return redirect(redirect_url())


#Пересканировать все доступные кэп
@app.route('/update', methods=['POST'])
@login_required
def update():
    subprocess.Popen(r'python /root/cryptobro_linux/absorb.py', shell=True)

    return redirect(redirect_url())


#функция скачивания кэпа
@app.route('/download', methods=['POST'])
@login_required
def download_file():
    # Очищаем имя папки с кэп-ом от служебных символов пути.
    # Т.к. полное имя имеет вид "HDIMAGE\\aphytbjg.000\041A" мы отрезаем всё лишнее чтобы
    # получилось "aphytbjg.000". Переменная paramfix2 содержать будет правильное имя
    paramfix = request.form['param']
    paramfix1 = paramfix[9:]             #количество символов "отрезаемых" слева
    paramfix2 = paramfix1[:-5]           #количество символов "отрезаемых" справа
    # перед скачиванием пакуем нужную папку с кэп-ом в зип архив:
    fantasy_zip = zipfile.ZipFile('/var/opt/cprocsp/keys/root/'+paramfix2+'.zip', 'w')
    for folder, subfolders, files in os.walk('/var/opt/cprocsp/keys/root/'+ paramfix2+''):
        for file in files:
           if file.endswith('.key'):
                fantasy_zip.write(os.path.join(folder, file),
                                  os.path.relpath(os.path.join(folder, paramfix2, file), '/var/opt/cprocsp/keys/root/'+ paramfix2+''),
                                  compress_type=zipfile.ZIP_DEFLATED)
    fantasy_zip.close()
    # Качаем получившийся зип:
    filename = paramfix2 + ".zip"
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename, as_attachment=True)

#функция удаления кэпа
@app.route('/delete', methods=['POST'])
@login_required
def delete_file():
    keyid = request.form['keyid']
    container_name = request.form['container']
    paramfix1 = container_name[9:]  # количество символов "отрезаемых" слева
    paramfix2 = paramfix1[:-5]  # количество символов "отрезаемых" справа
    if request.method == "POST":
        db = sqlite3.connect(r'/root/cryptobro_linux/DB/certs.db')
        db.execute('delete from article where keyid="' + keyid + '"')
        db.commit()
        if paramfix2 == '':  # Если имя контейнера будет пустым, удалится вся папка с ключами (лол) поэтому защищаемся вот этим
            return "Значение параметра container_name неверное. Чаще всего это возникает из-за загрузки в базу ключа с некорректным именем и последующей попыткой его удалить. Произведено удаление из установленных сертификатов, но на диске могут остаться куски ключа"
        else:
            subprocess.Popen(r'/opt/cprocsp/bin/amd64/certmgr -del -keyid ' + keyid, shell=True) #Удаляем из установленных сертификатов
            subprocess.Popen(r'rm -r /var/opt/cprocsp/keys/root/' + paramfix2, shell=True) #Удаляем каталог с ключом
            subprocess.Popen(r'rm /var/opt/cprocsp/keys/root/' + paramfix2 + '.zip', shell=True) #Удаляем zip-архив с ключом
        return redirect('/')
    else:
            return "При удалении произошла ошибка"


#функция обслуживания базы
@app.route('/delete_old_records', methods=['POST'])
@login_required
def delete_old_records():
    articles = Article.query.all()
    one_year = datetime.utcnow() - relativedelta(years=1)
    for el in articles:
        if el.exp < one_year: #функция удаления всех кэпов старше прошлого года. Тоже что и удаление одного кэпа, но в цикле с условием

            paramfix1 = el.container[9:]  # количество символов "отрезаемых" слева
            paramfix2 = paramfix1[:-5] # количество символов "отрезаемых" справа
            db = sqlite3.connect(r'/root/cryptobro_linux/DB/certs.db')
            db.execute('delete from article where keyid="' + el.keyid + '"')
            db.commit()
            if paramfix2 != '':  # Если имя контейнера будет пустым, удалится вся папка с ключами (лол) поэтому защищаемся вот этим

                 subprocess.Popen(r'/opt/cprocsp/bin/amd64/certmgr -del -keyid ' + el.keyid,
                                 shell=True)  # Удаляем из установленных сертификатов
                 subprocess.Popen(r'rm -r /var/opt/cprocsp/keys/root/' + paramfix2,
                                  shell=True)  # Удаляем каталог с ключом
                 subprocess.Popen(r'rm /var/opt/cprocsp/keys/root/' + paramfix2 + '.zip',
                                  shell=True)  # Удаляем zip-архив с ключом




    return redirect('/')




if __name__ == "__main__":
    app.run(debug=True)
