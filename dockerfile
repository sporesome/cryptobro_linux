FROM ubuntu:20.04
ARG DEBIAN_FRONTEND=noninteractive
RUN apt update && apt install python-dev python3-pip gunicorn git lsb-core -y -qq
WORKDIR /root/
RUN git clone https://gitlab.com/sporesome/cryptobro_linux.git
WORKDIR /root/cryptobro_linux/
RUN tar -xf /root/cryptobro_linux/linux-amd64_deb.tgz
RUN /root/cryptobro_linux/linux-amd64_deb/install.sh
RUN dpkg -i /root/cryptobro_linux/linux-amd64_deb/lsb-cprocsp-kc2-64_4.0.9975-6_amd64.deb
RUN /opt/cprocsp/sbin/amd64/cpconfig -license -set 4040C-N0000-01TTP-NY66C-ANQY4
RUN /root/cryptobro_linux/linux-amd64_deb/install.sh
RUN rm /root/cryptobro_linux/linux-amd64_deb.tgz && rm -r /root/cryptobro_linux/linux-amd64_deb
RUN pip3 install flask_sqlalchemy python-dateutil~=2.7.3 Werkzeug~=2.0.2 Flask~=2.0.2
RUN touch /etc/cron.d/update_certs && echo "*/5 * * * * root /update_certs.sh" >> /etc/cron.d/update_certs
RUN chmod 0644 /etc/cron.d/update_certs
RUN touch /update_certs.sh && echo "python3 /root/cryptobro_linux/absorb.py" >> /update_certs.sh
RUN chmod +x /update_certs.sh
CMD cron && gunicorn -w 4 -b 0.0.0.0:80 app:app








